package com.example.madhuawade.newweatherapp;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Madhu Awade on 16-12-2017.
 */

public interface WeatherService {
    @GET("forecast/bf745dc3da80ab397c40e432d8d0390b/{latitude},{longitude}")
    Call<WeatherApiResponse> getWeather(@Path("latitude") double latitude, @Path("longitude") double longitude);
}
