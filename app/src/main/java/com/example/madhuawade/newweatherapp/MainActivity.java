package com.example.madhuawade.newweatherapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "MainActivity";

    private WeatherService service;
    private WeatherApiResponse weather;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //adding button to my app i.e get the button for app
        Button mybutton = (Button) findViewById(R.id.btn_id);
        //set the button i.e action to be defined when the user clicks the button
        mybutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                service = Apiclient.getApiclient().create(WeatherService.class);
                TextView txt= (TextView) findViewById(R.id.mytext_id);

                Call<WeatherApiResponse> call = service.getWeather(48.21, 16.37);
                call.enqueue(new Callback<WeatherApiResponse>() {
                    @Override
                    public void onResponse(Call<WeatherApiResponse> call, Response<WeatherApiResponse> response) {
                        WeatherApiResponse weather = response.body();
                        Log.v(TAG, weather.timezone);
                    }

                    @Override
                    public void onFailure(Call<WeatherApiResponse> call, Throwable t) {
                        Log.v(TAG, "Error: " + t.getMessage());
                    }
                });

                // String result = call.execute().body().toString();
                //txt.setText(result);

             /*   WeatherApiResponse weather = null;
                try {
                    weather = call.execute().body();
                    Log.d("timetone",weather.timezone);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.d("timetone",weather.timezone);
                */



                Log.i("weather app","This is my first app");


                //add pop up in the UI
                Toast.makeText(getApplicationContext(),"Processing",Toast.LENGTH_SHORT)
                        .show();



/*
                call.enqueue(new Callback<WeatherApiResponse>() {
                    @Override
                    public void onResponse(Call<WeatherApiResponse> call, Response<WeatherApiResponse> response) {
                        WeatherApiResponse  weather  = response.body();
                      //  WeatherApiResponse weather = call.execute().body();

                            Log.d("timetone",weather.timezone);
                          //  Log.d("summary",w.currently.temperature);

                    }

                    @Override
                    public void onFailure(Call<WeatherApiResponse> call, Throwable t) {
                        Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });*/





            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
