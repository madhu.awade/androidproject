package com.example.madhuawade.newweatherapp;

public class WeatherApiResponse {
    class Currently {
        public int time;
        public String summary;
        public String icon;
        public float precipIntensity;
        public float precipProbability;
        public String precipType;
        public float temperature;
        public float apparentTemperature;
        public float dewPoint;
        public float humidity;
        public float pressure;
        public float windSpeed;
        public float windGust;
        public float windBearing;
        public float cloudCover;
        public float uvIndex;
        public float visibility;
        public float ozone;
    }
    public float latitude;
    public float longitude;
    public String timezone;
    public Currently currently;



}
