package com.example.madhuawade.newweatherapp;

import java.util.prefs.BackingStoreException;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Madhu Awade on 16-12-2017.
 */

public class Apiclient {
    public static  Retrofit getApiclient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.darksky.net/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

}
